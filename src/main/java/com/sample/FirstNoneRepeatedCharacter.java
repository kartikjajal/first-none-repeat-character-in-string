package com.sample;

public class FirstNoneRepeatedCharacter {
	public static void main(String[] args) {

		System.out.println(new FirstNoneRepeatedCharacter()
				.findFirstNoneRepeatedCharacter("aabbccddeeffgjj"));
	}

	public char findFirstNoneRepeatedCharacter(String toFind) {
		while (!toFind.isEmpty()) {
			char ch = toFind.charAt(0);
			if (!isCharacterFound(ch, toFind = toFind.substring(1))) {
				return (ch);
			}
			toFind = toFind.replaceAll(String.valueOf(ch), "");
		}
		return '\u0000';
	}

	private static boolean isCharacterFound(char ch, String toFind) {
		return toFind.indexOf(ch) < 0 ? false : true;
	}
}
